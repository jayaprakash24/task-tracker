package com.prakash.spring.task.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "T_TASK")
@Data
@NoArgsConstructor
public class Task {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "TASK_ID")
  private long id;

  private String name;

  @ElementCollection
  @CollectionTable(name = "T_TAGS", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "tags")
  private List<String> tags;

  private Date startDate;

  private Date endDate;

  private String status;

  private String description;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "PROJECT_ID")
  private Project project;


  public Task(String name, List<String> tags, Date startDate, Date endDate, String status,
      String description) {
    this.name = name;
    this.tags = tags;
    this.startDate = startDate;
    this.endDate = endDate;
    this.status = status;
    this.description = description;
  }
}
