package com.prakash.spring.task;

import com.prakash.spring.task.model.Project;
import java.util.List;

public interface ProjectRepository {
   List<Project> findAll();
}
