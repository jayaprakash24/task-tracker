package com.prakash.spring.task.service;

import com.prakash.spring.task.ProjectRepository;
import com.prakash.spring.task.exception.NotImplementedException;
import com.prakash.spring.task.model.Project;
import java.util.List;

public class ProjectService {

  private ProjectRepository projectRepository;

  public ProjectService(ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  public List<Project> fetchAllProjects() {
    return projectRepository.findAll();
  }

  public void save() {
    throw new NotImplementedException();
  }
}
