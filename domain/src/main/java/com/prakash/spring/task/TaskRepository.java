package com.prakash.spring.task;

import com.prakash.spring.task.model.Task;
import java.util.List;

public interface TaskRepository {
  List<Task> findAllByProject(long projectId);
}
