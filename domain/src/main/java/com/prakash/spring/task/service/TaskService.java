package com.prakash.spring.task.service;

import com.prakash.spring.task.TaskRepository;
import com.prakash.spring.task.exception.NotImplementedException;
import com.prakash.spring.task.model.Task;
import java.util.List;

public class TaskService {

  private TaskRepository taskRepository;

  public TaskService(TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  public void save() {
    //taskRepository.save(task);
  }

  public List<Task> fetchAllTasks(long projectId) {
    return taskRepository.findAllByProject(projectId);
  }

}
