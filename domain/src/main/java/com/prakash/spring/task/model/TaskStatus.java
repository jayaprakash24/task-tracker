package com.prakash.spring.task.model;

public enum TaskStatus {
  ACTIVE,
  INPROGRESS,
  INACTIVE,
  DELETED,
  COMPLETED;
}
