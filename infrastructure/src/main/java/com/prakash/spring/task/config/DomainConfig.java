package com.prakash.spring.task.config;

import com.prakash.spring.task.ProjectRepository;
import com.prakash.spring.task.TaskRepository;
import com.prakash.spring.task.service.ProjectService;
import com.prakash.spring.task.service.TaskService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {

  @Bean
  public ProjectService projectService(ProjectRepository projectRepository) {
    return new ProjectService(projectRepository);
  }

  @Bean
  public TaskService taskService(TaskRepository taskRepository) {
    return new TaskService(taskRepository);
  }
}
