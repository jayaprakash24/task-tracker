package com.prakash.spring.task.repository;

import com.prakash.spring.task.TaskRepository;
import com.prakash.spring.task.model.Task;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TaskJpaRepository extends JpaRepository<Task, Long>, TaskRepository{
  @Query("select t from Task t where t.project.id = :projectId")
  List<Task> findAllByProject(@Param("projectId") Long projectId);
}
