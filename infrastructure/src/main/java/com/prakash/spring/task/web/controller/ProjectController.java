package com.prakash.spring.task.web.controller;

import com.prakash.spring.task.exception.NotImplementedException;
import com.prakash.spring.task.model.Project;
import com.prakash.spring.task.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/projects")
public class ProjectController {

  @Autowired
  ProjectService projectService;

  @PostMapping
  public ResponseEntity saveProject(@RequestBody Project project) {
    throw new NotImplementedException();
  }

  @GetMapping
  public ResponseEntity getAllProjects() {
    return ResponseEntity.ok(projectService.fetchAllProjects());
  }

  @PutMapping
  public ResponseEntity updateProject(@RequestBody Project project) {
    throw new NotImplementedException();
  }

}
