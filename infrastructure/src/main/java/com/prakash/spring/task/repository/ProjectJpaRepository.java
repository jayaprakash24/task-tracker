package com.prakash.spring.task.repository;

import com.prakash.spring.task.ProjectRepository;
import com.prakash.spring.task.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectJpaRepository extends JpaRepository<Project, Long>, ProjectRepository{
    Project findByName(String name);
}
