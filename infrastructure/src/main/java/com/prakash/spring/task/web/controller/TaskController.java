package com.prakash.spring.task.web.controller;

import com.prakash.spring.task.exception.NotImplementedException;
import com.prakash.spring.task.model.Task;
import com.prakash.spring.task.service.TaskService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/projects")
public class TaskController {

  @Autowired
  TaskService taskService;

  @GetMapping(path = "/{project}/tasks/{task}")
  public Task getTaskFromProject(@PathVariable("project") long projectId, @PathVariable("task") String task) {
    throw new NotImplementedException();
  }

  @PostMapping(path = "/{project}/tasks")
  public ResponseEntity saveTask(@PathVariable("project") long projectId, @RequestBody Task taskDto) {
    throw new NotImplementedException();
  }

  @GetMapping(path = "/{project}/tasks")
  public List<Task> get(@PathVariable("project") long projectId) {
    throw new NotImplementedException();
  }

  @PutMapping(path = "/{project}/tasks")
  public ResponseEntity updateTask(@PathVariable("project") long projectId, @RequestBody Task taskDto) {
    throw new NotImplementedException();
  }
}
