package com.prakash.spring.task.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.PriorityOrdered;

@Slf4j
@Configuration
public class BeanInfoConfig implements BeanFactoryPostProcessor, PriorityOrdered {


  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
      throws BeansException {
    //String[] dependentBeans = beanFactory.getDependentBeans("dataSource");

    StringBuilder sb = new StringBuilder();
    for(String bean: beanFactory.getBeanDefinitionNames()) {
      sb.append(bean).append("\n");
    }
    log.info("Beans Loaded" +sb);
  }

  @Override
  public int getOrder() {
    return PriorityOrdered.LOWEST_PRECEDENCE;
  }
}
